<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A13427">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>A briefe remembrance of all the English monarchs with their raignes, deaths, and places of buriall : from the Normans Conquest, vnto Our Most Gratious Soueraigne / by Iohn Taylor.</title>
    <author>Taylor, John, 1580-1653.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A13427 of text S1145 in the <ref target="http;//estc.bl.uk">English Short Title Catalog</ref> (STC 23738.5). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A13427</idno>
    <idno type="STC">STC 23738.5</idno>
    <idno type="STC">ESTC S1145</idno>
    <idno type="EEBO-CITATION">21464697</idno>
    <idno type="OCLC">ocm 21464697</idno>
    <idno type="VID">23996</idno>
    <idno type="PROQUESTGOID">2248564547</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A13427)</note>
    <note>Transcribed from: (Early English Books Online ; image set 23996)</note>
    <note>Images scanned from microfilm: (Early English books, 1475-1640 ; 1737:2)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>A briefe remembrance of all the English monarchs with their raignes, deaths, and places of buriall : from the Normans Conquest, vnto Our Most Gratious Soueraigne / by Iohn Taylor.</title>
      <author>Taylor, John, 1580-1653.</author>
     </titleStmt>
     <extent>1 sheet ([3] p.).</extent>
     <publicationStmt>
      <publisher>Printed by George Eld,</publisher>
      <pubPlace>London :</pubPlace>
      <date>1622.</date>
     </publicationStmt>
     <notesStmt>
      <note>Advertisement.</note>
      <note>"The sheet is divided into 4 quarters, the bottom 2 containing the title and verses on Prince Charles reimposed from [STC] 23738. The upper left has an engraving of the royal arms, while the upper right is cut away. It may have contained a full-length portrait of Prince Charles."--STC (2nd ed.).</note>
      <note>Reproduction of original in the British Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Great Britain -- Kings and rulers.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>A briefe remembrance of all the English monarchs with their raignes, deaths, and places of buriall : from the Normans Conquest, vnto Our Most Gratious</ep:title>
    <ep:author>Taylor, John</ep:author>
    <ep:publicationYear>1622</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>222</ep:wordCount>
    <ep:defectiveTokenCount>9</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>405.41</ep:defectRate>
    <ep:finalGrade>F</ep:finalGrade>
    <ep:defectRangePerGrade> The  rate of 405.41 defects per 10,000 words puts this text in the F category of texts with  100 or more defects per 10,000 words.</ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2007-08</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2007-08</date><label>Apex CoVantage</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2007-09</date><label>Elspeth Healey</label>
        Sampled and proofread
      </change>
   <change><date>2007-09</date><label>Elspeth Healey</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2008-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A13427-t">
  <front xml:id="A13427-e0">
   <div cert="high" type="illustration" xml:id="A13427-e10" xml:lang="fr">
    <pb facs="tcp:23996:1" xml:id="A13427-001-a"/>
    <p xml:id="A13427-e20">
     <figure xml:id="A13427-e30">
      <p xml:id="A13427-e40">
       <w lemma="honi" pos="ffr" xml:id="A13427-001-a-0010">HONI</w>
       <w lemma="soit" pos="ffr" xml:id="A13427-001-a-0020">SOIT</w>
       <w lemma="qvi" pos="ffr" xml:id="A13427-001-a-0030">QVI</w>
       <w lemma="maly" pos="ffr" xml:id="A13427-001-a-0040">MALY</w>
       <w lemma="pense" pos="ffr" xml:id="A13427-001-a-0050">PENSE</w>
       <pc unit="sentence" xml:id="A13427-001-a-0060">.</pc>
      </p>
     </figure>
    </p>
   </div>
   <div type="title_page" xml:id="A13427-e50">
    <p xml:id="A13427-e60">
     <w lemma="a" pos="d" xml:id="A13427-001-a-0070">A</w>
     <w lemma="brief" pos="j" reg="BRIEF" xml:id="A13427-001-a-0080">BRIEFE</w>
     <w lemma="remembrance" pos="n1" xml:id="A13427-001-a-0090">REMEMBRANCE</w>
     <w lemma="of" pos="acp" xml:id="A13427-001-a-0100">OF</w>
     <w lemma="all" pos="d" xml:id="A13427-001-a-0110">ALL</w>
     <w lemma="the" pos="d" xml:id="A13427-001-a-0120">THE</w>
     <w lemma="english" pos="jnn" xml:id="A13427-001-a-0130">ENGLISH</w>
     <hi xml:id="A13427-e70">
      <w lemma="monarch" pos="n2" reg="MONARCHES" xml:id="A13427-001-a-0140">MONARCHS</w>
      <pc xml:id="A13427-001-a-0150">,</pc>
     </hi>
     <w lemma="with" pos="acp" reg="With" xml:id="A13427-001-a-0160">VVith</w>
     <w lemma="their" pos="po" xml:id="A13427-001-a-0170">their</w>
     <w lemma="reign" pos="vvz" reg="reigns" xml:id="A13427-001-a-0180">raignes</w>
     <pc xml:id="A13427-001-a-0190">,</pc>
     <w lemma="death" pos="n2" xml:id="A13427-001-a-0200">deaths</w>
     <pc xml:id="A13427-001-a-0210">,</pc>
     <w lemma="and" pos="cc" xml:id="A13427-001-a-0220">and</w>
     <w lemma="place" pos="n2" xml:id="A13427-001-a-0230">places</w>
     <w lemma="of" pos="acp" xml:id="A13427-001-a-0240">of</w>
     <w lemma="burial" pos="n1" reg="burial" xml:id="A13427-001-a-0250">buriall</w>
     <pc xml:id="A13427-001-a-0260">:</pc>
     <w lemma="from" pos="acp" xml:id="A13427-001-a-0270">From</w>
     <w lemma="the" pos="d" xml:id="A13427-001-a-0280">the</w>
     <hi xml:id="A13427-e80">
      <w lemma="norman" pos="nn2" xml:id="A13427-001-a-0290">Normans</w>
      <w lemma="conquest" pos="n1" xml:id="A13427-001-a-0300">Conquest</w>
      <pc xml:id="A13427-001-a-0310">,</pc>
     </hi>
     <w lemma="unto" pos="acp" reg="unto" xml:id="A13427-001-a-0320">vnto</w>
     <w lemma="our" pos="po" xml:id="A13427-001-a-0330">our</w>
     <w lemma="most" pos="avs-d" xml:id="A13427-001-a-0340">most</w>
     <w lemma="gracious" pos="j" reg="gracious" xml:id="A13427-001-a-0350">gratious</w>
     <w lemma="sovereign" pos="n1-j" reg="Sovereign" xml:id="A13427-001-a-0360">Soueraigne</w>
     <pc unit="sentence" xml:id="A13427-001-a-0370">.</pc>
    </p>
    <p xml:id="A13427-e90">
     <w lemma="by" pos="acp" xml:id="A13427-001-a-0380">By</w>
     <w lemma="JOHN" pos="nn1" reg="JOHN" xml:id="A13427-001-a-0390">IOHN</w>
     <w lemma="TAYLOR" pos="nn1" xml:id="A13427-001-a-0400">TAYLOR</w>
     <pc unit="sentence" xml:id="A13427-001-a-0410">.</pc>
    </p>
    <p xml:id="A13427-e100">
     <hi xml:id="A13427-e110">
      <w lemma="LONDON" pos="nn1" xml:id="A13427-001-a-0420">LONDON</w>
      <pc xml:id="A13427-001-a-0430">,</pc>
     </hi>
     <w lemma="print" pos="vvn" xml:id="A13427-001-a-0440">Printed</w>
     <w lemma="by" pos="acp" xml:id="A13427-001-a-0450">by</w>
     <w lemma="GEORGE" pos="nn1" xml:id="A13427-001-a-0460">GEORGE</w>
     <w lemma="eld" pos="n1" xml:id="A13427-001-a-0470">ELD</w>
     <pc xml:id="A13427-001-a-0480">,</pc>
     <w lemma="1622." pos="crd" xml:id="A13427-001-a-0490">1622.</w>
     <pc unit="sentence" xml:id="A13427-001-a-0500"/>
    </p>
   </div>
  </front>
  <body xml:id="A13427-e120">
   <div type="poem" xml:id="A13427-e130">
    <head xml:id="A13427-e140">
     <w lemma="the" pos="d" xml:id="A13427-001-a-0510">The</w>
     <w lemma="most" pos="avs-d" xml:id="A13427-001-a-0520">most</w>
     <w lemma="illustrious" pos="j" xml:id="A13427-001-a-0530">Illustrious</w>
     <w lemma="prince" pos="n1" xml:id="A13427-001-a-0540">Prince</w>
     <w lemma="CHARLES" pos="nn1" xml:id="A13427-001-a-0550">CHARLES</w>
     <pc xml:id="A13427-001-a-0560">,</pc>
     <w lemma="prince" pos="n1" xml:id="A13427-001-a-0570">Prince</w>
     <w lemma="of" pos="acp" xml:id="A13427-001-a-0580">of</w>
     <w lemma="great" pos="j" xml:id="A13427-001-a-0590">Great</w>
     <hi xml:id="A13427-e150">
      <w lemma="Britain" pos="nn1" reg="Britain" xml:id="A13427-001-a-0600">Britaine</w>
     </hi>
     <w lemma="and" pos="cc" xml:id="A13427-001-a-0610">and</w>
     <hi xml:id="A13427-e160">
      <w lemma="Ireland" pos="nn1" xml:id="A13427-001-a-0620">Ireland</w>
      <pc xml:id="A13427-001-a-0630">,</pc>
     </hi>
     <w lemma="duke" pos="n1" xml:id="A13427-001-a-0640">Duke</w>
     <w lemma="of" pos="acp" xml:id="A13427-001-a-0650">of</w>
     <hi xml:id="A13427-e170">
      <w lemma="Cornwall" pos="nn1" xml:id="A13427-001-a-0660">Cornwall</w>
      <pc xml:id="A13427-001-a-0670">,</pc>
      <w lemma="York" pos="nn1" reg="York" xml:id="A13427-001-a-0680">Yorke</w>
      <pc xml:id="A13427-001-a-0690">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A13427-001-a-0700">and</w>
     <hi xml:id="A13427-e180">
      <w lemma="Albany" pos="nn1" xml:id="A13427-001-a-0710">Albany</w>
      <pc xml:id="A13427-001-a-0720">;</pc>
     </hi>
     <w lemma="earl" pos="n1" reg="Earl" xml:id="A13427-001-a-0730">Earle</w>
     <w lemma="of" pos="acp" xml:id="A13427-001-a-0740">of</w>
     <hi xml:id="A13427-e190">
      <w lemma="ch●ster" pos="n1" xml:id="A13427-001-a-0750">Ch●ster</w>
      <pc xml:id="A13427-001-a-0760">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A13427-001-a-0770">and</w>
     <w lemma="knight" pos="n1" xml:id="A13427-001-a-0780">Knight</w>
     <w lemma="of" pos="acp" xml:id="A13427-001-a-0790">of</w>
     <w lemma="the" pos="d" xml:id="A13427-001-a-0800">the</w>
     <w lemma="most" pos="avs-d" xml:id="A13427-001-a-0810">most</w>
     <w lemma="noble" pos="j" xml:id="A13427-001-a-0820">Noble</w>
     <w lemma="order" pos="n1" xml:id="A13427-001-a-0830">Order</w>
     <w lemma="of" pos="acp" xml:id="A13427-001-a-0840">of</w>
     <w lemma="the" pos="d" xml:id="A13427-001-a-0850">the</w>
     <w lemma="garter" pos="n1" xml:id="A13427-001-a-0860">Garter</w>
     <pc xml:id="A13427-001-a-0870">,</pc>
     <w lemma="etc." pos="ab" reg="etc." xml:id="A13427-001-a-0880">&amp;c.</w>
     <pc unit="sentence" xml:id="A13427-001-a-0890"/>
    </head>
    <lg xml:id="A13427-e200">
     <l xml:id="A13427-e210">
      <w lemma="illustrious" pos="j" xml:id="A13427-001-a-0900">ILlustrious</w>
      <w lemma="Offa" pos="nn1" reg="Offa" xml:id="A13427-001-a-0910">Off</w>
      <w lemma="spring" pos="n1" xml:id="A13427-001-a-0920">spring</w>
      <w lemma="of" pos="acp" xml:id="A13427-001-a-0930">of</w>
      <w lemma="most" pos="avs-d" xml:id="A13427-001-a-0940">most</w>
      <w lemma="glorious" pos="j" xml:id="A13427-001-a-0950">glorious</w>
      <w lemma="Sten●" pos="nn1" xml:id="A13427-001-a-0960">Sten●</w>
      <pc xml:id="A13427-001-a-0970">,</pc>
     </l>
     <l xml:id="A13427-e220">
      <w lemma="our" pos="po" xml:id="A13427-001-a-0980">Our</w>
      <w lemma="happy" pos="j" xml:id="A13427-001-a-0990">happy</w>
      <w lemma="hope" pos="n1" xml:id="A13427-001-a-1000">hope</w>
      <pc xml:id="A13427-001-a-1010">,</pc>
      <w lemma="our" pos="po" xml:id="A13427-001-a-1020">our</w>
      <w lemma="royal" pos="j" reg="Royal" xml:id="A13427-001-a-1030">Royall</w>
      <w lemma="CHARLES" pos="nn1" xml:id="A13427-001-a-1040">CHARLES</w>
      <w lemma="the" pos="d" xml:id="A13427-001-a-1050">the</w>
      <w lemma="great" pos="j" xml:id="A13427-001-a-1060">great</w>
      <pc xml:id="A13427-001-a-1070">,</pc>
     </l>
     <l xml:id="A13427-e230">
      <w lemma="successive" pos="j" reg="Successive" xml:id="A13427-001-a-1080">Successiue</w>
      <w lemma="H●y" pos="nn1" xml:id="A13427-001-a-1090">H●y</w>
      <w lemma="e" pos="sy" xml:id="A13427-001-a-1100">e</w>
      <w lemma="to" pos="acp" xml:id="A13427-001-a-1110">to</w>
      <w lemma="four" pos="crd" reg="four" xml:id="A13427-001-a-1120">foure</w>
      <w lemma="rich" pos="j" xml:id="A13427-001-a-1130">rich</w>
      <w lemma="diadem" pos="n2" xml:id="A13427-001-a-1140">Diadems</w>
      <pc xml:id="A13427-001-a-1150">,</pc>
     </l>
     <l xml:id="A13427-e240">
      <w lemma="with" pos="acp" xml:id="A13427-001-a-1160">With</w>
      <w lemma="gift" pos="n2" xml:id="A13427-001-a-1170">gifts</w>
      <w lemma="of" pos="acp" xml:id="A13427-001-a-1180">of</w>
      <w lemma="grace" pos="n1" xml:id="A13427-001-a-1190">Grace</w>
      <w lemma="and" pos="cc" xml:id="A13427-001-a-1200">and</w>
      <w lemma="learning" pos="n1" xml:id="A13427-001-a-1210">Learning</w>
      <w lemma="●●gh" pos="av-d" xml:id="A13427-001-a-1220">●●gh</w>
      <w lemma="replete" pos="j" reg="replete" xml:id="A13427-001-a-1230">repleat</w>
      <pc unit="sentence" xml:id="A13427-001-a-1240">.</pc>
     </l>
     <l xml:id="A13427-e250">
      <w lemma="for" pos="acp" xml:id="A13427-001-a-1250">For</w>
      <w lemma="thou" pos="pno" xml:id="A13427-001-a-1260">thee</w>
      <w lemma="the" pos="d" xml:id="A13427-001-a-1270">th'</w>
      <w lemma="almighty" pos="ng1-j" reg="Almighty's" xml:id="A13427-001-a-1280">Almighties</w>
      <w lemma="aid" pos="n1" xml:id="A13427-001-a-1290">aid</w>
      <w lemma="i" pos="pns" xml:id="A13427-001-a-1300">I</w>
      <w lemma="do" pos="vvb" reg="do" xml:id="A13427-001-a-1310">doe</w>
      <w lemma="m●●●at" pos="j" xml:id="A13427-001-a-1320">m●●●at</w>
      <pc xml:id="A13427-001-a-1330">,</pc>
     </l>
     <l xml:id="A13427-e260">
      <w lemma="to" pos="prt" xml:id="A13427-001-a-1340">To</w>
      <w lemma="guide" pos="vvi" xml:id="A13427-001-a-1350">guide</w>
      <w lemma="and" pos="cc" xml:id="A13427-001-a-1360">and</w>
      <w lemma="prosper" pos="vvi" xml:id="A13427-001-a-1370">prosper</w>
      <w lemma="thy" pos="po" xml:id="A13427-001-a-1380">thy</w>
      <w lemma="proceed" pos="n2-vg" xml:id="A13427-001-a-1390">proceedings</w>
      <w lemma="still" pos="av" xml:id="A13427-001-a-1400">still</w>
      <pc xml:id="A13427-001-a-1410">,</pc>
     </l>
     <l xml:id="A13427-e270">
      <w lemma="that" pos="cs" xml:id="A13427-001-a-1420">That</w>
      <w lemma="long" pos="av-j" xml:id="A13427-001-a-1430">long</w>
      <w lemma="thou" pos="pns" xml:id="A13427-001-a-1440">thou</w>
      <w lemma="may" pos="vm2" reg="mayst" xml:id="A13427-001-a-1450">maist</w>
      <w lemma="survive" pos="vvi" reg="survive" xml:id="A13427-001-a-1460">suruiue</w>
      <w lemma="a" pos="d" xml:id="A13427-001-a-1470">a</w>
      <w lemma="prince" pos="n1" xml:id="A13427-001-a-1480">Prince</w>
      <w lemma="complete" pos="j" reg="complete" xml:id="A13427-001-a-1490">compleat</w>
      <pc xml:id="A13427-001-a-1500">,</pc>
     </l>
     <l xml:id="A13427-e280">
      <w lemma="to" pos="prt" xml:id="A13427-001-a-1510">To</w>
      <w lemma="guard" pos="vvi" xml:id="A13427-001-a-1520">guard</w>
      <w lemma="the" pos="d" xml:id="A13427-001-a-1530">the</w>
      <w lemma="good" pos="j" xml:id="A13427-001-a-1540">good</w>
      <pc xml:id="A13427-001-a-1550">,</pc>
      <w lemma="and" pos="cc" xml:id="A13427-001-a-1560">and</w>
      <w lemma="to" pos="prt" xml:id="A13427-001-a-1570">to</w>
      <w lemma="subvert" pos="vvi" reg="subvert" xml:id="A13427-001-a-1580">subuert</w>
      <w lemma="the" pos="d" xml:id="A13427-001-a-1590">the</w>
      <w lemma="ill" pos="j" xml:id="A13427-001-a-1600">ill</w>
      <pc unit="sentence" xml:id="A13427-001-a-1610">.</pc>
     </l>
     <l xml:id="A13427-e290">
      <w lemma="and" pos="cc" xml:id="A13427-001-a-1620">And</w>
      <w lemma="when" pos="crq" xml:id="A13427-001-a-1630">when</w>
      <pc join="right" xml:id="A13427-001-a-1640">(</pc>
      <w lemma="by" pos="acp" xml:id="A13427-001-a-1650">by</w>
      <w lemma="god" pos="n2" xml:id="A13427-001-a-1660">Gods</w>
      <w lemma="determine" pos="vvn" reg="determined" xml:id="A13427-001-a-1670">determin'd</w>
      <w lemma="boundless" pos="j" reg="boundless" xml:id="A13427-001-a-1680">boundlesse</w>
      <w lemma="will" pos="n1" xml:id="A13427-001-a-1690">will</w>
      <pc xml:id="A13427-001-a-1700">)</pc>
     </l>
     <l xml:id="A13427-e300">
      <w lemma="thy" pos="po" xml:id="A13427-001-a-1710">Thy</w>
      <w lemma="gracious" pos="j" reg="gracious" xml:id="A13427-001-a-1720">gra●ious</w>
      <w lemma="father" pos="n1" xml:id="A13427-001-a-1730">Father</w>
      <w lemma="shall" pos="vmb" xml:id="A13427-001-a-1740">shall</w>
      <w lemma="immortal" pos="j" reg="immortal" xml:id="A13427-001-a-1750">immortall</w>
      <w lemma="be" pos="vvi" xml:id="A13427-001-a-1760">be</w>
      <pc xml:id="A13427-001-a-1770">,</pc>
     </l>
     <l xml:id="A13427-e310">
      <w lemma="then" pos="av" xml:id="A13427-001-a-1780">Then</w>
      <w lemma="let" pos="vvb" xml:id="A13427-001-a-1790">let</w>
      <w lemma="thy" pos="po" xml:id="A13427-001-a-1800">thy</w>
      <w lemma="〈◊〉" pos="zz" xml:id="A13427-001-a-1810">〈◊〉</w>
      <pc join="right" xml:id="A13427-001-a-1820">(</pc>
      <w lemma="●ike" pos="j" xml:id="A13427-001-a-1830">●ike</w>
      <w lemma="he" pos="png" xml:id="A13427-001-a-1840">his</w>
      <pc xml:id="A13427-001-a-1850">)</pc>
      <w lemma="the" pos="d" xml:id="A13427-001-a-1860">the</w>
      <w lemma="world" pos="n1" xml:id="A13427-001-a-1870">world</w>
      <w lemma="fulfil" pos="vvi" reg="fulfil" xml:id="A13427-001-a-1880">fulfill</w>
      <pc xml:id="A13427-001-a-1890">,</pc>
     </l>
     <l xml:id="A13427-e320">
      <w lemma="that" pos="cs" xml:id="A13427-001-a-1900">That</w>
      <w lemma="thou" pos="pns" xml:id="A13427-001-a-1910">thou</w>
      <w lemma="may" pos="vm2" reg="mayst" xml:id="A13427-001-a-1920">maist</w>
      <w lemma="●oy" pos="vvi" xml:id="A13427-001-a-1930">●oy</w>
      <w lemma="in" pos="acp" xml:id="A13427-001-a-1940">in</w>
      <w lemma="we" pos="pno" reg="us" xml:id="A13427-001-a-1950">vs</w>
      <pc xml:id="A13427-001-a-1960">,</pc>
      <w lemma="and" pos="cc" xml:id="A13427-001-a-1970">and</w>
      <w lemma="we" pos="pns" xml:id="A13427-001-a-1980">we</w>
      <w lemma="in" pos="acp" xml:id="A13427-001-a-1990">in</w>
      <w lemma="thou" pos="pno" xml:id="A13427-001-a-2000">thee</w>
      <pc unit="sentence" xml:id="A13427-001-a-2010">.</pc>
     </l>
     <l xml:id="A13427-e330">
      <w lemma="and" pos="cc" xml:id="A13427-001-a-2020">And</w>
      <w lemma="all" pos="d" xml:id="A13427-001-a-2030">all</w>
      <w lemma="true" pos="j" xml:id="A13427-001-a-2040">true</w>
      <hi xml:id="A13427-e340">
       <w lemma="Britain" pos="nng1" reg="Britain's" xml:id="A13427-001-a-2050">Britaines</w>
      </hi>
      <w lemma="prey" pos="n1" reg="prey" xml:id="A13427-001-a-2060">pray</w>
      <w lemma="to" pos="acp" xml:id="A13427-001-a-2070">to</w>
      <w lemma="God" pos="nn1" xml:id="A13427-001-a-2080">God</w>
      <w lemma="above" pos="acp" reg="above" xml:id="A13427-001-a-2090">aboue</w>
      <pc xml:id="A13427-001-a-2100">,</pc>
     </l>
     <l xml:id="A13427-e350">
      <w lemma="to" pos="prt" xml:id="A13427-001-a-2110">To</w>
      <w lemma="match" pos="vvi" xml:id="A13427-001-a-2120">match</w>
      <w lemma="thy" pos="po" xml:id="A13427-001-a-2130">thy</w>
      <w lemma="life" pos="n1" xml:id="A13427-001-a-2140">life</w>
      <w lemma="and" pos="cc" xml:id="A13427-001-a-2150">and</w>
      <w lemma="fortune" pos="n1" xml:id="A13427-001-a-2160">fortune</w>
      <w lemma="with" pos="acp" xml:id="A13427-001-a-2170">with</w>
      <w lemma="their" pos="po" xml:id="A13427-001-a-2180">their</w>
      <w lemma="love" pos="n1" reg="love" xml:id="A13427-001-a-2190">loue</w>
      <pc xml:id="A13427-001-a-2200">,</pc>
     </l>
    </lg>
    <trailer xml:id="A13427-e360">
     <w lemma="finis" pos="fla" xml:id="A13427-001-a-2210">FINIS</w>
     <pc unit="sentence" xml:id="A13427-001-a-2220">.</pc>
    </trailer>
   </div>
  </body>
 </text>
</TEI>
